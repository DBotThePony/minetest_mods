
--  Copyright (C) 2018 DBot

--  Licensed under the Apache License, Version 2.0 (the "License")
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at

--    http:www.apache.org/licenses/LICENSE-2.0

--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.

local woodtypes = {}
local axetypes = {}
local leavetypes = {
	'default:pine_needles',
}

local function setting(name, ifNone)
	local grab = minetest.setting_getbool(name)

	if grab == nil then
		return ifNone
	end

	return grab
end

local function fastfind(strIn, pattern)
	return string.find(strIn, pattern, 1, true)
end

local nodes = core.registered_nodes

local function isTree(classname)
	if setting('timber_nodenames', false) then
		return fastfind(classname, '_tree') or
			fastfind(classname, 'tree_') or
			fastfind(classname, ':tree') or
			woodtypes[classname]
	else
		if not nodes[classname] then return end
		if not nodes[classname].groups then return end
		return type(nodes[classname].groups.tree) == 'number' and nodes[classname].groups.tree > 0
	end
end

local function isAxe(itemstack)
	if itemstack:is_empty() then return false end

	if setting('timber_toolnames', false) then
		local classname = itemstack:get_name()
		return fastfind(classname, '_axe') or
			fastfind(classname, 'axe_') or
			fastfind(classname, ':axe') or
			axetypes[classname]
	else
		local def = itemstack:get_definition()
		if not def.tool_capabilities then return false end
		if not def.tool_capabilities.groupcaps then return false end
		if not def.tool_capabilities.groupcaps.choppy then return false end
		return def.tool_capabilities.groupcaps.choppy.uses and def.tool_capabilities.groupcaps.choppy.uses > 0
	end
end

local function isLeaves(classname)
	if setting('timber_nodenames', false) then
		return fastfind(classname, '_leaves') or
			fastfind(classname, 'leaves_') or
			fastfind(classname, ':leaves') or
			fastfind(classname, 'apple') or
			fastfind(classname, '_needles') or leavetypes[classname]
	else
		if not nodes[classname] then return end
		if not nodes[classname].groups then return end
		return type(nodes[classname].groups.leaves) == 'number' and nodes[classname].groups.leaves > 0 or
			type(nodes[classname].groups.leafdecay) == 'number' and nodes[classname].groups.leafdecay > 0
	end
end

local FIND_LEAVES = {}

local directions = {
	{1, 0, 0},
	{-1, 0, 0},
	{0, 1, 0},
	{0, -1, 0},
	{0, 0, 1},
	{0, 0, -1},
}

local function posstr(pos)
	return string.format('%i_%i_%i', pos.x, pos.y, pos.z)
end

local function distanceBetwen(pos1, pos2)
	return math.sqrt(math.pow(pos1.x - pos2.x, 2) + math.pow(pos1.y - pos2.y, 2) + math.pow(pos1.z - pos2.z, 2))
end

local function findLeaves(pos, initialPos, allowedDistance, ignore)
	if allowedDistance <= 0 or distanceBetwen(pos, initialPos) >= allowedDistance then return end
	if FIND_LEAVES[posstr(pos)] then return end

	if not ignore then
		local getnode = minetest.get_node(pos)
		if not getnode or getnode.name == 'ignore' or not isLeaves(getnode.name) then return end

		FIND_LEAVES[posstr(pos)] = {pos, getnode}
	end

	for i, shift in ipairs(directions) do
		local newpos = vector.new(pos.x + shift[1], pos.y + shift[2], pos.z + shift[3])
		findLeaves(newpos, initialPos, allowedDistance, ply, false)
	end
end

local function chopDown(pos, ply)
	for z = -1, 1 do
		for y = -1, 1 do
			for x = -1, 1 do
				local newpos = vector.new(pos.x + x, pos.y + y, pos.z + z)
				local node = minetest.get_node(newpos)

				if node and node.name ~= 'ignore' and isTree(node.name) then
					findLeaves(newpos, newpos, 5, true)
					minetest.node_dig(newpos, node, ply)

					local stack = ply:get_wielded_item()
					if not isAxe(stack) or stack:get_count() == 0 then return end -- halt, our tool broke!
					chopDown(newpos, ply)
				end
			end
		end
	end
end

local RUNNING = false

minetest.register_on_dignode(function(pos, node, ply)
	if RUNNING then return end
	if not isTree(node.name) then return end

	local stack = ply:get_wielded_item()
	if not isAxe(stack) then return end

	RUNNING = true
	--local t1 = os.clock()
	chopDown(pos, ply)

	local stack = ply:get_wielded_item()
	if not isAxe(stack) or stack:get_count() == 0 then return end

	for position, data in pairs(FIND_LEAVES) do
		minetest.node_dig(data[1], data[2], ply)
		ply:set_wielded_item(stack)
	end

	--print(string.format('Elapsed time on timber: %.2fms', (os.clock() - t1) * 1000))
	FIND_LEAVES = {}
	RUNNING = false
end)
