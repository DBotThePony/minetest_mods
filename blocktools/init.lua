
--  Copyright (C) 2018 DBot

--  Licensed under the Apache License, Version 2.0 (the "License")
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at

--    http:www.apache.org/licenses/LICENSE-2.0

--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.

local registered_items = core.registered_items

local function copy(target)
	assert(type(target) == 'table', 'Copy - input is not a table! typeof ' .. type(target))

	local tab = {}

	for k, v in pairs(target) do
		if type(v) == 'table' then
			tab[k] = copy(v)
		else
			tab[k] = v
		end
	end

	return tab
end

local function doRegister(name, recipeOf)
	local grabDefault = registered_items['default:' .. name]

	if not grabDefault then
		error('default:' .. name .. ' is MISSING! Load cannot continue')
	end

	local internalName = 'blocktools:' .. name
	local clone = copy(grabDefault)
	local tool_capabilities = clone.tool_capabilities

	if type(tool_capabilities.groupcaps) == 'table' then
		for k, v in pairs(tool_capabilities.groupcaps) do
			if type(v) == 'table' then
				if v.uses then
					v.uses = v.uses * 9
				end

				if type(v.times) == 'table' then
					for index, time in ipairs(v.times) do
						v.times[index] = time * 0.85
					end
				end
			end
		end
	end

	if type(tool_capabilities.full_punch_interval) == 'number' then
		tool_capabilities.full_punch_interval = tool_capabilities.full_punch_interval * 0.9
	end

	clone.description = 'Block ' .. clone.description
	clone.tool_capabilities = tool_capabilities

	minetest.register_tool(internalName, clone)

	return internalName
end

local function pickaxe(name, recipeOf)
	local internalName = doRegister('pick_' .. name, recipeOf)

	minetest.register_craft({
		output = internalName,
		recipe = {
			{recipeOf, 	recipeOf, 			recipeOf},
			{'', 		'default:stick', 	''},
			{'', 		'default:stick', 	''},
		}
	})

	return internalName
end

local function shovel(name, recipeOf)
	local internalName = doRegister('shovel_' .. name, recipeOf)

	minetest.register_craft({
		output = internalName,
		recipe = {
			{'', 	recipeOf, 			''},
			{'', 	'default:stick', 	''},
			{'', 	'default:stick', 	''},
		}
	})

	return internalName
end

local function axe(name, recipeOf)
	local internalName = doRegister('axe_' .. name, recipeOf)

	minetest.register_craft({
		output = internalName,
		recipe = {
			{recipeOf, 	recipeOf, 			''},
			{recipeOf, 	'default:stick', 	''},
			{'', 		'default:stick', 	''},
		}
	})

	minetest.register_craft({
		output = internalName,
		recipe = {
			{'', 		recipeOf, 			recipeOf},
			{'', 		'default:stick', 	recipeOf},
			{'', 		'default:stick', 	''},
		}
	})

	return internalName
end

pickaxe('steel', 'default:steelblock')
pickaxe('bronze', 'default:bronzeblock')
pickaxe('mese', 'default:mese')
pickaxe('diamond', 'default:diamondblock')

shovel('steel', 'default:steelblock')
shovel('bronze', 'default:bronzeblock')
shovel('mese', 'default:mese')
shovel('diamond', 'default:diamondblock')

axe('steel', 'default:steelblock')
axe('bronze', 'default:bronzeblock')
axe('mese', 'default:mese')
axe('diamond', 'default:diamondblock')
