
--  Copyright (C) 2018 DBot

--  Licensed under the Apache License, Version 2.0 (the "License")
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at

--    http:www.apache.org/licenses/LICENSE-2.0

--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.

local function fastfind(strIn, pattern)
	return string.find(strIn, pattern, 1, true)
end

local SKIP = false
local SKIP_NEXT = false

for nodename, definition in pairs(core.registered_nodes) do
	if fastfind(nodename, 'doors:') then
		local old_on_rightclick = definition.on_rightclick

		local newdef = {
			on_rightclick = function(pos, node, ply, itemstack, pointed_thing, ...)
				if SKIP_NEXT then
					SKIP_NEXT = false
					return old_on_rightclick(pos, node, ply, itemstack, pointed_thing, ...)
				end

				if SKIP then return old_on_rightclick(pos, node, ply, itemstack, pointed_thing, ...) end
				if ply and ply:is_player() and ply:get_player_control().sneak then return old_on_rightclick(pos, node, ply, itemstack, pointed_thing, ...) end
				SKIP = true

				--print(minetest.get_node(pos).name)
				--print(dump(pos))

				local hit = false

				--print('----')
				for z = -3, 3 do
					for x = -3, 3 do
						if x ~= 0 or z ~= 0 then
							local newpos = vector.new(pos.x + x, pos.y, pos.z + z)
							local node2 = minetest.get_node(newpos)

							--print(node2.name)

							if node2 and node2.name ~= 'ignore' and fastfind(node2.name, 'doors:') and core.registered_nodes[node2.name] then
								core.registered_nodes[node2.name].on_rightclick(newpos, node2, ply, itemstack, pointed_thing, ...)
								hit = true
								break
							end
						end
					end

					if hit then
						break
					end
				end

				SKIP = false
				SKIP_NEXT = true
				return old_on_rightclick(pos, node, ply, itemstack, pointed_thing, ...)
			end
		}

		core.override_item(nodename, newdef)
	end
end
