
--  Copyright (C) 2018 DBot

--  Licensed under the Apache License, Version 2.0 (the "License")
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at

--    http:www.apache.org/licenses/LICENSE-2.0

--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.

local registered_items = core.registered_items

local function fastfind(strIn, pattern)
	return string.find(strIn, pattern, 1, true)
end

local torch = registered_items['default:torch']

for itemname, definition in pairs(registered_items) do
	if fastfind(itemname, 'pick_') or fastfind(itemname, '_pick') or fastfind(itemname, ':pick') then
		local oldFunc = definition.on_place

		core.override_item(itemname, {
			on_place = function(itemstack, ply, pointed_thing, ...)
				if not ply then
					if oldFunc then
						return oldFunc(itemstack, ply, pointed_thing, ...)
					end

					return
				end

				local under = pointed_thing.under
				local node = minetest.get_node(under)
				local def = minetest.registered_nodes[node.name]

				if def and def.on_rightclick and not (ply and ply:is_player() and ply:get_player_control().sneak) then
					return def.on_rightclick(under, node, ply, itemstack, pointed_thing) or itemstack
				end

				local inventory = ply:get_inventory()

				if inventory then
					local lists = inventory:get_lists()

					if lists then
						local torchStack, torchStackIndex, listIn

						for listName, itemStacks in pairs(lists) do
							for index, itemStack in ipairs(itemStacks) do
								if not itemStack:is_empty() and itemStack:get_name() == 'default:torch' then
									torchStack = itemStack
									torchStackIndex = index
									listIn = listName
									break
								end
							end
						end

						if torchStack then
							--torchStack:take_item()
							--minetest.item_place(torchStack, ply, pointed_thing)
							torch.on_place(torchStack, ply, pointed_thing, ...)
							inventory:set_stack(listIn, torchStackIndex, torchStack)
						end
					end
				end

				if oldFunc then
					return oldFunc(itemstack, ply, pointed_thing, ...)
				end
			end,
		})
	end
end
