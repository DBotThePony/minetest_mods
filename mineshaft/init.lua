
--  Copyright (C) 2018 DBot

--  Licensed under the Apache License, Version 2.0 (the "License")
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at

--    http:www.apache.org/licenses/LICENSE-2.0

--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.

mineshaft_mod = {}
minetest.register_privilege('mineshaft', 'Allow player to use mineshaft commands')

local enumnext = 0
local function enum()
	enumnext = enumnext + 1
	return enumnext - 1
end

mineshaft_mod.TYPE_STRIPMINE = enum()
mineshaft_mod.TYPE_STRIPMINE3 = enum()
mineshaft_mod.TYPE_STRIPMINE_GUIDE = enum()
mineshaft_mod.TYPE_STRIPMINE3_GUIDE = enum()

local incomingDig = {}
local pickaxetypes = {}

local _FOLDER = minetest.get_modpath('mineshafts')
dofile(_FOLDER .. '/functions.lua')
dofile(_FOLDER .. '/mineshafts.lua')

minetest.register_on_dignode(function(pos, node, ply)
	if not ply then return end
	local nick = ply:get_player_name()
	if not incomingDig[nick] then return end

	if not incomingDig[nick].pointer then
		print('[Mineshaft] ' .. nick .. ' did not had Pointer defined!')
		minetest.chat_send_player(nick, 'Internal error of Pointer!')
		return
	end

	local stack = ply:get_wielded_item()
	if not mineshaft_mod.isPickaxe(stack) then return end

	local data = incomingDig[nick]
	incomingDig[nick] = nil
	local typeIn = data.type

	if typeIn == mineshaft_mod.TYPE_STRIPMINE then
		mineshaft_mod.digStripMine(ply, pos, data.length)
	elseif typeIn == mineshaft_mod.TYPE_STRIPMINE3 then
		mineshaft_mod.digStripMine3(ply, pos, data.length)
	elseif typeIn == mineshaft_mod.TYPE_STRIPMINE3_GUIDE then
		mineshaft_mod.digStripMine3Guide(ply, pos, data.length)
	elseif typeIn == mineshaft_mod.TYPE_STRIPMINE_GUIDE then
		mineshaft_mod.digStripMineGuide(ply, pos, data.length)
	end

	minetest.chat_send_player(nick, 'Digged up!')
end)

minetest.register_on_punchnode(function(pos, node, ply, pointer)
	if not ply then return end
	if not incomingDig[ply:get_player_name()] then return end
	incomingDig[ply:get_player_name()].pointer = pointer
end)

local function checkLen(lengthIn)
	return lengthIn and lengthIn > 3 and (not mineshaft_mod.setting('mineshaft_limit', true) or mineshaft_mod.setting('mineshaft_limitnum', 100) >= lengthIn)
end

minetest.register_chatcommand('dig', {
	params = '<type> [length] [wide?]',
	description = 'Dig mineshafts easily! Avaliable types are "stripmine", "stripmine3" and their "_guide" variants, length is depth of mine (default is 15), wide is only avaliable for "quarry"',

	privs = {mineshaft = true},

	func = function(nickname, params)
		local ply = minetest.get_player_by_name(nickname)

		if not ply then
			return false, 'Command is player-only'
		end

		if params == '' then
			return false, 'None arguments were provided'
		end

		local args = {}

		for str in string.gmatch(params, '([0-9a-zA-Z_-]+)') do
			table.insert(args, str)
		end

		local mineshaftType = args[1]
		local length = tonumber(args[2] or 15)

		if mineshaftType == 'stripmine' or mineshaftType == 'strip' then
			if not checkLen(length) then
				return false, 'Invalid length!'
			end

			incomingDig[nickname] = {
				type = mineshaft_mod.TYPE_STRIPMINE,
				length = length
			}

			return true, 'Next dig using pickaxe will dig a strip mine!'
		elseif mineshaftType == 'stripmine3' or mineshaftType == 'strip3' then
			if not checkLen(length) then
				return false, 'Invalid length!'
			end

			incomingDig[nickname] = {
				type = mineshaft_mod.TYPE_STRIPMINE3,
				length = length
			}

			return true, 'Next dig using pickaxe will dig a strip 3 block height mine!'
		elseif mineshaftType == 'stripmine3_guide' or mineshaftType == 'strip3_guide' then
			if not checkLen(length) then
				return false, 'Invalid length!'
			end

			incomingDig[nickname] = {
				type = mineshaft_mod.TYPE_STRIPMINE3_GUIDE,
				length = length
			}

			return true, 'Next dig using pickaxe will dig a guide strip 3 block height mine!'
		elseif mineshaftType == 'stripmine_guide' or mineshaftType == 'strip_guide' then
			if not checkLen(length) then
				return false, 'Invalid length!'
			end

			incomingDig[nickname] = {
				type = mineshaft_mod.TYPE_STRIPMINE_GUIDE,
				length = length
			}

			return true, 'Next dig using pickaxe will dig a guide strip mine!'
		else
			return false, 'Invalid mineshaft type. Use /help dig'
		end
	end
})
