
--  Copyright (C) 2018 DBot

--  Licensed under the Apache License, Version 2.0 (the "License")
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at

--    http:www.apache.org/licenses/LICENSE-2.0

--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.

function mineshaft_mod.tryToDig(ply, positions)
	for i, pos in ipairs(positions) do
		local node = minetest.get_node(pos)

		if node and node.name ~= 'ignore' then
			minetest.node_dig(pos, node, ply)
			local stack = ply:get_wielded_item()
			if not mineshaft_mod.isPickaxe(stack) or stack:get_count() == 0 then return false end
		end
	end

	return true
end

function mineshaft_mod.fastfind(strIn, pattern)
	return string.find(strIn, pattern, 1, true)
end

function mineshaft_mod.setting(name, ifNone)
	local grab = minetest.setting_getbool(name)

	if grab == nil then
		return ifNone
	end

	return grab
end

function mineshaft_mod.isPickaxe(itemstack)
	if itemstack:is_empty() then return false end

	if mineshaft_mod.setting('mineshaft_toolnames', false) then
		local classname = itemstack:get_name()
		return mineshaft_mod.fastfind(classname, '_pickaxe') or
			mineshaft_mod.fastfind(classname, 'pickaxe_') or
			mineshaft_mod.fastfind(classname, ':pickaxe') or
			mineshaft_mod.fastfind(classname, ':pick_') or
			pickaxetypes[classname]
	else
		local def = itemstack:get_definition()
		if not def.tool_capabilities then return false end
		if not def.tool_capabilities.groupcaps then return false end
		if not def.tool_capabilities.groupcaps.cracky then return false end
		return def.tool_capabilities.groupcaps.cracky and def.tool_capabilities.groupcaps.cracky.uses > 0
	end
end

function mineshaft_mod.getDirection(ply, pos, mult, add)
	local yaw = math.deg(ply:get_look_horizontal())
	add = add or 0
	yaw = yaw + add

	if yaw < 0 then
		yaw = 360 + yaw
	elseif yaw > 360 then
		yaw = yaw % 360
	end

	mult = mult or 1

	if yaw >= 315 or yaw <= 45 then
		return vector.new(pos.x, pos.y, pos.z + mult)
	elseif yaw >= 45 and yaw <= 135 then
		return vector.new(pos.x - mult, pos.y, pos.z)
	elseif yaw >= 135 and yaw <= 225 then
		return vector.new(pos.x, pos.y, pos.z - mult)
	else
		return vector.new(pos.x + mult, pos.y, pos.z)
	end
end

local torch = core.registered_items['default:torch']

function mineshaft_mod.findAndPlaceTorch(ply, posAt, pointedThing)
	if not pointedThing then
		pointedThing = {
			type = 'node',
			above = vector.new(posAt.x, posAt.y - 1, posAt.z),
			under = vector.new(posAt)
			--ref = ply
		}
	end

	local inventory = ply:get_inventory()

	if inventory then
		local lists = inventory:get_lists()

		if lists then
			local torchStack, torchStackIndex, listIn

			for listName, itemStacks in pairs(lists) do
				for index, itemStack in ipairs(itemStacks) do
					if not itemStack:is_empty() and itemStack:get_name() == 'default:torch' then
						torchStack = itemStack
						torchStackIndex = index
						listIn = listName
						break
					end
				end
			end

			if torchStack then
				--torchStack:take_item()
				--minetest.item_place(torchStack, ply, pointed_thing)
				torch.on_place(torchStack, ply, pointedThing)
				inventory:set_stack(listIn, torchStackIndex, torchStack)
				return true
			end

			return false
		end

		return false
	end

	return false
end
