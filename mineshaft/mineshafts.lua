
--  Copyright (C) 2018 DBot

--  Licensed under the Apache License, Version 2.0 (the "License")
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at

--    http:www.apache.org/licenses/LICENSE-2.0

--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.

function mineshaft_mod.digStripMine(ply, pos, length)
	for i = 0, length do
		local newpos = mineshaft_mod.getDirection(ply, pos, i)
		local newpos2 = vector.new(newpos.x, newpos.y - 1, newpos.z)

		if mineshaft_mod.tryToDig(ply, {newpos, newpos2}) and i % 5 == 0 and i ~= 0 then
			mineshaft_mod.findAndPlaceTorch(ply, vector.new(newpos.x, newpos.y + 1, newpos.z))
		end
	end
end

function mineshaft_mod.digStripMineGuide(ply, pos, length)
	for i = 0, length do
		local newpos = mineshaft_mod.getDirection(ply, pos, i)
		local newpos2 = vector.new(newpos.x, newpos.y - 1, newpos.z)

		local diggy = {newpos, newpos2}

		if i % 3 == 0 and i ~= 0 then
			table.insert(diggy, mineshaft_mod.getDirection(ply, newpos, 1, 90))
			table.insert(diggy, mineshaft_mod.getDirection(ply, newpos, 1, -90))
			table.insert(diggy, mineshaft_mod.getDirection(ply, newpos2, 1, 90))
			table.insert(diggy, mineshaft_mod.getDirection(ply, newpos2, 1, -90))
		end

		if mineshaft_mod.tryToDig(ply, diggy) and i % 5 == 0 and i ~= 0 then
			mineshaft_mod.findAndPlaceTorch(ply, vector.new(newpos.x, newpos.y + 1, newpos.z))
		end
	end
end

function mineshaft_mod.digStripMine3(ply, pos, length)
	for i = 0, length do
		local newpos = mineshaft_mod.getDirection(ply, pos, i)
		local newpos2 = vector.new(newpos.x, newpos.y - 1, newpos.z)
		local newpos3 = vector.new(newpos.x, newpos.y + 1, newpos.z)

		if mineshaft_mod.tryToDig(ply, {newpos, newpos2, newpos3}) and i % 5 == 0 and i ~= 0 then
			mineshaft_mod.findAndPlaceTorch(ply, vector.new(newpos.x, newpos.y + 2, newpos.z))
		end
	end
end

function mineshaft_mod.digStripMine3Guide(ply, pos, length)
	for i = 0, length do
		local newpos = mineshaft_mod.getDirection(ply, pos, i)
		local newpos2 = vector.new(newpos.x, newpos.y - 1, newpos.z)
		local newpos3 = vector.new(newpos.x, newpos.y + 1, newpos.z)

		local diggy = {newpos, newpos2, newpos3}

		if i % 3 == 0 and i ~= 0 then
			table.insert(diggy, mineshaft_mod.getDirection(ply, newpos, 1, 90))
			table.insert(diggy, mineshaft_mod.getDirection(ply, newpos, 1, -90))
			table.insert(diggy, mineshaft_mod.getDirection(ply, newpos2, 1, 90))
			table.insert(diggy, mineshaft_mod.getDirection(ply, newpos2, 1, -90))
			table.insert(diggy, mineshaft_mod.getDirection(ply, newpos3, 1, 90))
			table.insert(diggy, mineshaft_mod.getDirection(ply, newpos3, 1, -90))
		end

		if mineshaft_mod.tryToDig(ply, diggy) and i % 5 == 0 and i ~= 0 then
			mineshaft_mod.findAndPlaceTorch(ply, vector.new(newpos.x, newpos.y + 2, newpos.z))
		end
	end
end
