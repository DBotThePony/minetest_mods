
--  Copyright (C) 2018 DBot

--  Licensed under the Apache License, Version 2.0 (the "License")
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at

--    http:www.apache.org/licenses/LICENSE-2.0

--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.

local registered_items = core.registered_items

-- initial data fixes
-- probably too bad
--[[
for itemname, definiton in pairs(registered_items) do
	if type(definiton.stack_max) == 'number' and definiton.stack_max == 99 then
		core.override_item(itemname, {
			stack_max = 200,
		})
	end
end
]]

-- specific data fixes
core.override_item('default:torch', {
	stack_max = 999,
})

local function fastfind(strIn, pattern)
	return string.find(strIn, pattern, 1, true)
end

for itemname, definiton in pairs(registered_items) do
	if fastfind(itemname, '_lump') or fastfind(itemname, 'lump_') then
		core.override_item(itemname, {
			stack_max = not fastfind(itemname, 'coal') and 400 or 800,
		})
	end

	if fastfind(itemname, 'obsidian') then
		core.override_item(itemname, {
			stack_max = 200,
		})
	end

	if fastfind(itemname, 'clay') then
		core.override_item(itemname, {
			stack_max = 400,
		})
	end

	if fastfind(itemname, 'flint') or fastfind(itemname, 'stick') then
		core.override_item(itemname, {
			stack_max = 999,
		})
	end

	if fastfind(itemname, '_ingot') or fastfind(itemname, 'ingot_') then
		core.override_item(itemname, {
			stack_max = 800,
		})
	end

	if definiton.type == 'node' then
		if fastfind(itemname, 'stone') or fastfind(itemname, 'cobble') or fastfind(itemname, 'sand') or fastfind(itemname, 'gravel') then
			core.override_item(itemname, {
				stack_max = 500,
			})
		end

		if (fastfind(itemname, 'wood_') or fastfind(itemname, '_wood') or fastfind(itemname, 'glass')) or fastfind(itemname, 'brick') then
			core.override_item(itemname, {
				stack_max = 750,
			})
		end

		if fastfind(itemname, 'tree') then
			core.override_item(itemname, {
				stack_max = 200,
			})
		end

		if fastfind(itemname, 'leaves') or fastfind(itemname, '_needles') then
			core.override_item(itemname, {
				stack_max = 600,
			})
		end
	end
end
