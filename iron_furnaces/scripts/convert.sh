#!/bin/bash

blocktypes=( "bronze" "copper" "diamond" "mese" "steel" "tin" "gold" )

for blocktype in ${blocktypes[@]}
do
	convert "src/default_${blocktype}_block.png" -compose bumpmap src/default_furnace_front.png -gravity center -composite "output/${blocktype}_furnace_front.png"
	convert "src/default_${blocktype}_block.png" -compose bumpmap src/default_furnace_side.png -gravity center -composite "output/${blocktype}_furnace_side.png"
	convert "src/default_${blocktype}_block.png" -compose bumpmap src/default_furnace_top.png -gravity center -composite "output/${blocktype}_furnace_top.png"
	convert "src/default_${blocktype}_block.png" -compose bumpmap src/default_furnace_bottom.png -gravity center -composite "output/${blocktype}_furnace_bottom.png"

	convert "src/default_${blocktype}_block.png" \( -clone 0 \) \( -clone 0 \) \( -clone 0 \) \( -clone 0 \) \( -clone 0 \) \( -clone 0 \) \( -clone 0 \) \
	-append -compose bumpmap src/default_furnace_front_active.png -gravity center \
	-composite "output/${blocktype}_furnace_active.png"
done
