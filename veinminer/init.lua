
--  Copyright (C) 2018 DBot

--  Licensed under the Apache License, Version 2.0 (the "License")
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at

--    http:www.apache.org/licenses/LICENSE-2.0

--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.

local oretypes = {
	['default:obsidian'] = true,
}

local pickaxetypes = {}

local shovelthings = {
	['default:clay'] = true,
}

local shovels = {}
local shovelLimits = {
	['default:sand'] = 60,
}

local function fastfind(strIn, pattern)
	return string.find(strIn, pattern, 1, true)
end

local function setting(name, ifNone)
	local grab = minetest.setting_getbool(name)

	if grab == nil then
		return ifNone
	end

	return grab
end

local registry

local function isOre(classname)
	if setting('veinminer_nodenames', false) then
		return fastfind(classname, '_ore') or
			fastfind(classname, 'ore_') or
			fastfind(classname, 'stone_with_') or
			fastfind(classname, '_coal') or
			fastfind(classname, '_iron') or
			fastfind(classname, '_copper') or
			fastfind(classname, '_steel') or
			fastfind(classname, '_gold') or
			fastfind(classname, '_diamond') or
			fastfind(classname, '_diamonds') or
			fastfind(classname, '_silver') or
			fastfind(classname, ':ore') or
			oretypes[classname]
	else
		if not registry then
			registry = {}

			for index, oredef in pairs(core.registered_ores) do
				if type(oredef) == 'table' and oredef.ore then
					registry[oredef.ore] = true
				end
			end
		end

		return oretypes[classname] == true or registry[classname] == true
	end
end

local function isAcceptableShovel(classname)
	return fastfind(classname, 'silver_sand') or
		fastfind(classname, '_gravel') or
		fastfind(classname, 'gravel_') or
		fastfind(classname, ':gravel') or
		fastfind(classname, ':sand') or
		shovelthings[classname]
end

local function isPickaxe(itemstack)
	if itemstack:is_empty() then return false end

	if setting('veinminer_toolnames', false) then
		local classname = itemstack:get_name()
		return fastfind(classname, '_pickaxe') or
			fastfind(classname, 'pickaxe_') or
			fastfind(classname, ':pickaxe') or
			fastfind(classname, ':pick_') or
			pickaxetypes[classname]
	else
		local def = itemstack:get_definition()
		if not def.tool_capabilities then return false end
		if not def.tool_capabilities.groupcaps then return false end
		if not def.tool_capabilities.groupcaps.cracky then return false end
		return def.tool_capabilities.groupcaps.cracky and def.tool_capabilities.groupcaps.cracky.uses > 0
	end
end

local function isShovel(itemstack)
	if itemstack:is_empty() then return false end

	if setting('veinminer_toolnames', false) then
		return fastfind(classname, '_shovel') or
			fastfind(classname, 'shovel_') or
			fastfind(classname, ':shovel') or
			pickaxetypes[classname]
	else
		local def = itemstack:get_definition()
		if not def.tool_capabilities then return false end
		if not def.tool_capabilities.groupcaps then return false end
		if not def.tool_capabilities.groupcaps.crumbly then return false end
		return def.tool_capabilities.groupcaps.crumbly and def.tool_capabilities.groupcaps.crumbly.uses > 0
	end
end

local function distanceBetwen(pos1, pos2)
	return math.sqrt(math.pow(pos1.x - pos2.x, 2) + math.pow(pos1.y - pos2.y, 2) + math.pow(pos1.z - pos2.z, 2))
end

local function digOres(name, pos, ply)
	for z = -1, 1 do
		for y = -1, 1 do
			for x = -1, 1 do
				local newpos = vector.new(pos.x + x, pos.y + y, pos.z + z)
				local node = minetest.get_node(newpos)

				if node and node.name ~= 'ignore' and isOre(node.name) and node.name == name then
					minetest.node_dig(newpos, node, ply)

					local stack = ply:get_wielded_item()
					if not isPickaxe(stack) or stack:get_count() == 0 then return end -- halt, our tool broke!
					digOres(name, newpos, ply)
				end
			end
		end
	end
end

local FIND_SHOVEL = {}
local CHECK_SHOVEL = {}
local SHOVEL_LIMIT = -1

local function digShovel(name, pos, ply)
	if SHOVEL_LIMIT <= 0 then return end
	local xyz = string.format('%i_%i_%i', pos.x, pos.y, pos.z)
	if CHECK_SHOVEL[xyz] then return end

	CHECK_SHOVEL[xyz] = true

	for z = -1, 1 do
		for y = -1, 1 do
			for x = -1, 1 do
				local newpos = vector.new(pos.x + x, pos.y + y, pos.z + z)
				local node = minetest.get_node(newpos)

				if node and node.name ~= 'ignore' and isAcceptableShovel(node.name) and node.name == name then
					local xyz = string.format('%i_%i_%i', newpos.x, newpos.y, newpos.z)
					--minetest.node_dig(newpos, node, ply)
					SHOVEL_LIMIT = SHOVEL_LIMIT - 1
					if SHOVEL_LIMIT <= 0 then return end
					FIND_SHOVEL[xyz] = {newpos, node}

					--local stack = ply:get_wielded_item()
					--if stack:is_empty() or not isShovel(stack:get_name()) or stack:get_count() == 0 then return end -- halt, our tool broke!
					digShovel(name, newpos, ply)
				end
			end
		end
	end
end

local function doDigPickaxe(pos, node, ply)
	if not isOre(node.name) then return end

	local stack = ply:get_wielded_item()
	if not isPickaxe(stack) then return end

	if not setting("veinminer_enable_pickaxe", true) then return end
	digOres(node.name, pos, ply)
end

local function doDigShovel(pos, node, ply)
	if not isAcceptableShovel(node.name) then return end

	local stack = ply:get_wielded_item()
	if not isShovel(stack) then return end

	if not setting("veinminer_enable_shovel", true) then return end
	local limit = shovelLimits[node.name] or 99
	SHOVEL_LIMIT = limit
	digShovel(node.name, pos, ply)

	local tosort = {}

	for xyz, data in pairs(FIND_SHOVEL) do
		table.insert(tosort, data)
	end

	FIND_SHOVEL = {}
	CHECK_SHOVEL = {}

	table.sort(tosort, function(a, b)
		return a[1].y > b[1].y
	end)

	for i, data in ipairs(tosort) do
		minetest.node_dig(data[1], data[2], ply)

		local stack = ply:get_wielded_item()
		if not isShovel(stack) or stack:get_count() == 0 then return end -- halt, our tool broke!
	end
end

local RUNNING = false

minetest.register_on_dignode(function(pos, node, ply)
	if not ply then return end

	if not setting("veinminer_enable", true) then return end
	if RUNNING then return end
	if not node or not node.name or node.name == 'ignore' then return end

	RUNNING = true
	doDigPickaxe(pos, node, ply)
	doDigShovel(pos, node, ply)
	RUNNING = false
end)
