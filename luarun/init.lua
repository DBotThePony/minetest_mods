
--  Copyright (C) 2018 DBot

--  Licensed under the Apache License, Version 2.0 (the "License")
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at

--    http:www.apache.org/licenses/LICENSE-2.0

--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.

minetest.register_privilege('luarun', 'Allow player to execute lua code')

minetest.register_chatcommand('luarun', {
	params = '<code>',
	description = 'Executes lua code on serverside.',

	privs = {luarun = true},

	func = function(nickname, params)
		local ply = minetest.get_player_by_name(nickname)

		if params == '' then
			return false, 'Empty script'
		end

		local env = getfenv and getfenv(1) or _G or _ENV

		if not env then
			return false, 'No candidate for Lua environment is avaliable'
		end

		local thing = loadstring('return ' .. params)

		if setfenv then
			setfenv(thing, env)
		end

		local trace
		local status, executionThing = xpcall(thing, function(err)
			trace = debug.traceback(err)
		end)

		if status then
			return true, '< ' .. params .. '\n> ' .. dump(executionThing)
		else
			return false, '< ' .. params .. '\n' .. trace
		end
	end
})

minetest.register_chatcommand('reloadmod', {
	params = '<modname>',
	description = 'Reloads a mod on serverside. USE WITH CAUTION. WORKS ONLY WITH VERY PLAIN SIMPLE SERVER-SIDE MODS.',

	privs = {luarun = true},

	func = function(nickname, params)
		local ply = minetest.get_player_by_name(nickname)

		if params == '' then
			return false, 'No mod name'
		end

		local _FOLDER = minetest.get_modpath(params)

		if not _FOLDER then
			return false, 'Invalid mod folder specified'
		end

		local trace
		local status = xpcall(dofile, function(err) trace = debug.traceback(err) end, _FOLDER .. '/init.lua')

		if status then
			return true, 'Mod probably reloaded successfully'
		else
			return false, trace
		end
	end
})
